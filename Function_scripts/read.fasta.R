###name

###input

###output

###Description

read.fasta <- function(file){
                      if(missing(file)){                                          stop("provide .fasta file to read")}
                      ###Load the data
                      data.tmp <-read.table(file,sep="\r")
                      data.tmp <- as.matrix(data.tmp)

                      ###If some lines are separated by \n, sort those out
                      while(length(grep("\n",data.tmp)) > 0){
                          foldups <- grep("\n",data.tmp)
                          if(length(foldups)>0){
                              for(i in 1:length(foldups)){
                                  if(i == 1){
                                      data.fin <- data.tmp[1:foldups[i]-1]
                                  }
                                  if(i > 1){
                                      data.fin <- c(data.fin,data.tmp[(foldups[i-1]+1):(foldups[i]-1)])
                                  }

                                  data.fin <- c(data.fin,unlist(strsplit(data.tmp[foldups[i]],split="\n")))

                                  if(i == length(foldups) & foldups[i] != length(data.tmp)){
                                      data.fin <- c(data.fin,data.tmp[(foldups[i]+1):length(data.tmp)])
                                  }
                              }
                              data.tmp <- data.fin
                              ###Delete empty lines
                              if(sum(nchar(data.tmp) == 0) > 0){
                                  data.tmp <- data.tmp[-which(nchar(data.tmp)==0)]
                              }
                          }
                      }


                      ###Find the descriptor lines
                      descriptors.loc <- grep(">",data.tmp)
                      descriptors <- data.tmp[descriptors.loc]

                      ###combine the sequences
                      seq.out <- NULL
                      for(i in 1:length(descriptors.loc)){
                          if(i == length(descriptors.loc)){
                              data.nu <- data.tmp[(descriptors.loc[i]+1):length(data.tmp)]
                          }
                          if(i < length(descriptors.loc)){
                              data.nu <- data.tmp[(descriptors.loc[i]+1):(descriptors.loc[i+1]-1)]
                          }
                          seq.nu <- NULL
                          for(j in 1:length(data.nu)){
                                  seq.nu <- paste(seq.nu,data.nu[j],sep="")
                          }
                          seq.out <- c(seq.out,seq.nu)
                      }

                      ###Make a package
                      output <- cbind(descriptors,toupper(seq.out))
                      return(output)
                     }

