###function for single marker mapping
#Snoek & Sterken, 2017; Sterken, 2017

###input
#map1.dataframe; output of the QTL.map1.dataframe function
#threshold; the significance treshold for calling QTL (in -log10(p))
#LOD.drop; the decrease in LOD-score compared to the peak to call the boundaries of the QTL. Standard is 1.5

###output
#the original dataframe with the added columns: qtl_peak, qtl_marker_left, qtl_bp_left, qtl_marker_right, qtl_bp_right
#these columns are only non-NA where a QTL peak is located.

###Description
#This takes output of mapping.to.list function and identiefies peaks and confidence intervals v1.3
#Based on a given threshold and a 1.5 LOD-drop.
#v1.2 made the function more efficient, the peak borders are selected in one processing
#v1.3 fixed the problem calling the peak if the peak borders the edge of the chromosome
#v1.4 fixed 'larger then, smaller then' bug
#v1.5 fixed 'incompatible types' bug; NA is not recognized as numeric

QTL.peak.finder <- function(map1.dataframe,threshold,LOD.drop){
                            if(missing(map1.dataframe)){            stop("Missing map1.dataframe (trait, qtl_chromosome, qtl_bp, qtl_marker, qtl_significance, qtl_effect)")}
                            if(missing(threshold)){            stop("Missing threshold")}
                            if(missing(LOD.drop)){             LOD.drop <- 1.5}

                            ###locate the peak
                            peaks <- dplyr::filter(map1.dataframe, qtl_significance >= threshold-LOD.drop) %>%
                                     dplyr::group_by(trait,qtl_chromosome) %>%
                                     dplyr::filter(qtl_significance == max(qtl_significance),max(qtl_significance)>=threshold) %>%
                                     dplyr::filter(abs(qtl_marker - mean(qtl_marker)) == min(abs(qtl_marker - mean(qtl_marker)))) %>%
                                     dplyr::filter(qtl_marker == min(qtl_marker)) %>%
                                     as.data.frame() %>%
                                     dplyr::mutate(qtl_peak=qtl_marker)

                            ###locate left boundary
                            peaks_left <- dplyr::filter(map1.dataframe,trait %in% unique(peaks$trait)) %>%
                                          dplyr::group_by(trait,qtl_chromosome) %>%
                                          dplyr::mutate(Border=ifelse((qtl_significance < max(qtl_significance)-LOD.drop & max(qtl_significance)>=threshold),T,
                                                                 ifelse((qtl_bp == min(qtl_bp) & min(qtl_bp) %in% qtl_bp[qtl_significance >= max(qtl_significance)-LOD.drop & max(qtl_significance)>=threshold]),T,
                                                                   ifelse((qtl_bp == max(qtl_bp) & max(qtl_bp) %in% qtl_bp[qtl_significance >= max(qtl_significance)-LOD.drop & max(qtl_significance)>=threshold]),T,F))),
                                                        peakloc=qtl_marker[which.max(qtl_significance)]) %>%
                                          dplyr::filter(Border) %>%
                                          dplyr::mutate(qtl_marker_left=as.numeric(ifelse(qtl_marker == max(qtl_marker[qtl_marker <= peakloc]),qtl_marker,NA)),
                                                        qtl_bp_left=as.numeric(ifelse(qtl_bp == max(qtl_bp[qtl_marker <= peakloc]),qtl_bp,NA)),
                                                        qtl_marker_right=as.numeric(ifelse(qtl_marker == min(qtl_marker[qtl_marker >= peakloc]),qtl_marker,NA)),
                                                        qtl_bp_right=as.numeric(ifelse(qtl_bp == min(qtl_bp[qtl_marker >= peakloc]),qtl_bp,NA))) %>%
                                          dplyr::filter(!is.na(qtl_bp_left) | !is.na(qtl_bp_right)) %>%
                                          dplyr::select(-Border,-peakloc)

                            peaks_right <- dplyr::filter(peaks_left,!is.na(qtl_bp_right))
                            peaks_left <- dplyr::filter(peaks_left,!is.na(qtl_bp_left))

                            ###Merge the findings
                            peaks <- cbind(peaks,peaks_left$qtl_marker_left,peaks_left$qtl_bp_left,peaks_right$qtl_marker_right,peaks_right$qtl_bp_right)
                            ###Prepare input
                            output <- dplyr::mutate(map1.dataframe,qtl_peak = NA,qtl_marker_left = NA,qtl_bp_left = NA,qtl_marker_right = NA,qtl_bp_right = NA)
                            ###Merge peaks and input
                            output[match(paste(peaks[,1],peaks[,2],peaks[,3]),paste(output[,1],output[,2],output[,3])),7:11] <- peaks[,7:11]
                            output <- dplyr::mutate(output,qtl_bp_left=ifelse(qtl_bp_left > qtl_bp,qtl_bp,qtl_bp_left),qtl_bp_right=ifelse(qtl_bp_right<qtl_bp,qtl_bp,qtl_bp_right))

                            ###return called peaks
                            return(output)
                           }

